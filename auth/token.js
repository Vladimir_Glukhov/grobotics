const jwt = require('jsonwebtoken');
const config = require('../config/config');

const Token = (req, res, next) => {
    const token = req.headers['x-access-token'];
    if (!token) {
        return res.status(403).send({ auth: false, message: 'No token provided' })
    }
    jwt.verify(token, config.key, async (err, decoded) => {
        if (err) return res.status(404).send('Error from server');
        req.user = decoded;
        console.log('im in token');
        //res.status(200).send(decoded);
        return next();
    });

}

module.exports = Token;