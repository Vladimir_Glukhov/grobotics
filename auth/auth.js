const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const config = require('../config/config');
const jwt = require('jsonwebtoken');

const token = require('./token');

module.exports = function (app) {
    app.route('/api/login').post(jsonParser, (req, res) => {
        if (!req.body) return res.status(403).send('Error request');
        const correctPassword = config.key;
        const correctLogin = config.login;
        if (req.body.password !== correctPassword || req.body.login !== correctLogin) return res.status(401).send({ auth: false, token: null });
        const token = jwt.sign({ login: config.login }, config.key, { expiresIn: 600000 });
        res.status(200).send({ auth: true, token });
    });

    app.route('/api/logout').get((req, res) => {
        res.status(200).send({ auth: false, token: null });
    });

    app.route('/api/user').get(token, (req, res, next) => {
        res.status(200).send({ auth: true, login: config.login });
        return next();
    });
};