const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');

const app = express();
const server = require('http').createServer(app);
const jwt = require('jsonwebtoken');

const authRouter = require('../auth/auth');

// enabled cors origins
const allowed = ['http://localhost:8080'];

console.log('separated server started');

const verifyToken = (req, res, next) => {
  const token = req.body.token || req.query.token || req.headers['x-access-token'];

  if (!token) {
    return res.status(403).send('no token');
  }
  try {
    const decoded = jwt.verify(token, '123');
    req.user = decoded;
    return next();
  } catch (err) {
    return res.status(401).send('invalid token');
  }
};

const { connectedDevicesSocketId, accessToUser } = require('../socket/api');

const port = process.env.PORT || 3000;

const myPath = express.static(path.join(__dirname, '../build'));

app.use(bodyParser.json({ type: 'application/*+json' }));
app.use(cors({ origin: allowed }));
app.use(myPath);

app.get('/device', verifyToken, async (req, res, next) => {
  const { deviceId } = req.query;
  const userId = req.user && req.user.login;

  // const devSocketId = await connectedDevicesSocketId.get(deviceId);
  // const userIdSuccess = await accessToUser.getAccess(deviceId);//add cach

  res.status(200).send(['CAM', 'MOT']);
  // if (userIdSuccess === userId) {
  //     res.status(200).send(devSets[deviceId]);
  // } else {
  //     res.status(403);
  // }
  return next();
});

authRouter(app);

app.get(/^\/(?!api)(?!device).*$/, (req, res) => {
  res.sendFile(path.join(__dirname, '../build/index.html'));
});

server.listen(port);
