const found = (array) => {
    let isFirstEven = array[0] % 2 === 0;
    for (let i = 1; i < array.length; i += 1) {
        let currentEven = array[i] % 2 === 0 ;
        if (currentEven === isFirstEven) {
            currentEven = !isFirstEven;
        }
        if ((array[i] % 2 === 0) === currentEven) {
            return array[i]; }
    }
}