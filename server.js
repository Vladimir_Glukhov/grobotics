const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');

const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');

const authRouter = require('./auth/auth');

const devSets = {};
const usersSocketIds = [];

// enabled cors origins
const allowed = ['http://localhost:8080'];

const verifyToken = (req, res, next) => {
  const token = req.body.token || req.query.token || req.headers['x-access-token'];

  if (!token) {
    return res.status(403).send('no token');
  }
  try {
    const decoded = jwt.verify(token, '123');
    req.user = decoded;
    return next();
  } catch (err) {
    return res.status(401).send('invalid token');
  }
};

const { connectedDevicesSocketId, connectedUsersSocketId, accessToUser } = require('./api');

const port = process.env.PORT || 3000;

const cameraResp = {};

const myPath = express.static(path.join(__dirname, 'build'));

app.use(bodyParser.json({ type: 'application/*+json' }));

app.use(cors({ origin: allowed }));

app.get('/roboland/controllers/camera.mjpg', (req, res) => {
  console.log(req.body);
  res.writeHead(200, {
    // 'Cache-Control': 'no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0',
    'Cache-Control': 'no-cache, no-store, must-revalidate',
    Pragma: 'no-cache',
    'Pragma-Directive': 'no-cache',
    'Cache-Directive': 'no-cache',
    Connection: 'close',
    Expires: '0',
    'Content-Type': 'multipart/x-mixed-replace; boundary=--myboundary',
  });
  cameraResp.user_id = res;
});

app.use(myPath);

app.get('/device', verifyToken, async (req, res, next) => {
  const { deviceId } = req.query;
  const userId = req.user && req.user.login;

  const devSocketId = await connectedDevicesSocketId.get(deviceId);
  const userIdSuccess = await accessToUser.getAccess(deviceId);// add cach

  // res.status(200).send(['CAM', 'MOT']);
  if (userIdSuccess === userId) {
    res.status(200).send(devSets[deviceId]);
  } else {
    res.status(403);
  }
  return next();
});

authRouter(app);

app.get(/^\/(?!api)(?!device).*$/, (req, res) => {
  console.log('request', req.path);
  res.sendFile(path.join(__dirname, '/build/index.html'));
});

server.listen(port);

const devIdsSet = {};
console.log('DEPRECATED');
io.on('connection', (socket) => {
  console.log('connected', socket.id);
  socket.on('send', async (userId, deviceId, msg) => {
    const devId = await connectedDevicesSocketId.get(deviceId);
    if (!devId) {
      console.log('device not found');
      return;
    }
    const userIdSuccess = await accessToUser.getAccess(deviceId);// add cach

    if (userIdSuccess === userId) {
      socket.to(devId).emit('to_dev', msg);
    } else {
      console.log('access denied');
    }
  });
  socket.on('device_connect', async (devId, description) => {
    devIdsSet[socket.id] = devId;
    devSets[devId] = description;
    // get ip address
    // console.log(socket.request.connection.remoteAddress);
    setDeviceStatus(socket, devId, true);

    return connectedDevicesSocketId.add(devId, socket.id);
  });
  socket.on('init_user', async (userId, devId) => {
    await connectedUsersSocketId.add(userId, socket.id);
    const devSocketId = await connectedDevicesSocketId.get(devId);
    socket.to(devSocketId).emit('init_user', userId);
  });
  socket.on('init_user_success', async (userId, devId) => {
    await accessToUser.addAccess(devId, userId);
    const userSocketId = await connectedUsersSocketId.get(userId);
    const devSocketId = await connectedDevicesSocketId.get(devId);
    console.log('init_user_success', devSocketId);
    socket.to(userSocketId).emit('set_user_status', true);
    // socket.to(userSocketId).emit('device_socket_id', devSocketId);
  });
  socket.on('msg_from_device', async (userId, msg) => {
    const userSocketId = await connectedUsersSocketId.get(userId);
    // console.log('msg_from_device', msg);
    // console.log(`send_device_message:${msg.deviceId}`)
    socket.to(userSocketId).emit(`send_device_message:${msg.deviceId}`, userId, msg);
  });
  socket.on('msg_rob', async (userId, msg) => {
    const userSocketId = await connectedUsersSocketId.get(userId);
    // console.log('msg_from_device', msg);
    // console.log(`send_device_message:${msg.deviceId}`)
    socket.to(userSocketId).emit('msg_rob', msg);
  });

  socket.on('msg_cam', (userId, msg) => {
    if (cameraResp.user_id) {
      const ids = connectedUsersSocketId.getAll();
      cameraResp.user_id.write(`--myboundary\nContent-Type: image/jpg\nContent-length: ${msg.length}\n\n`);
      cameraResp.user_id.write(msg, () => { });
    }
  });

  socket.on('get_ngrok_url', async (userId, devId) => {
    console.log('get ngrok url', devId);
    const devSocketId = await connectedDevicesSocketId.get(devId);
    socket.to(devSocketId).emit('get_ngrok_url', userId);
  });
  socket.on('init_ngrok_url', async (userId, msg) => {
    console.log('init_ngrok_url', msg);
    const userSocketId = await connectedUsersSocketId.get(userId);
    socket.to(userSocketId).emit('recive_ngrok_url', msg);
  });

  socket.on('disconnect', async (...ar) => {
    console.log('disconnected', socket.id);
    const devId = devIdsSet[socket.id];
    delete devIdsSet[socket.id];
    connectedUsersSocketId.delete(socket.id);
    setDeviceStatus(socket, devId, false);
  });

  /*
    socket.on('disconnect', () => {
        console.log(`[${socketToRoom[socket.id]}]: ${socket.id} exit`);
        const roomID = socketToRoom[socket.id];
        let room = users[roomID];
        if (room) {
            room = room.filter(user => user.id !== socket.id);
            users[roomID] = room;
            if (room.length === 0) {
                delete users[roomID];
                return;
            }
        }
        socket.to(roomID).emit('user_exit', {id: socket.id});
        console.log(users);
    })

    /////RTCPeer part

     socket.on('offer', data => {
        //console.log(data.sdp);
        socket.to(data.offerReceiveID).emit('getOffer', {sdp: data.sdp, offerSendID: data.offerSendID, offerSendEmail: data.offerSendEmail});
    });

    socket.on('answer', data => {
        //console.log(data.sdp);
        socket.to(data.answerReceiveID).emit('getAnswer', {sdp: data.sdp, answerSendID: data.answerSendID});
    });

    socket.on('candidate', data => {
        //console.log(data.candidate);
        socket.to(data.candidateReceiveID).emit('getCandidate', {candidate: data.candidate, candidateSendID: data.candidateSendID});
    })

    socket.on('join_room', data => {
        if (users[data.room]) {
            const length = users[data.room].length;
            if (length === maximum) {
                socket.to(socket.id).emit('room_full');
                return;
            }
            users[data.room].push({id: socket.id, email: data.email});
        } else {
            users[data.room] = [{id: socket.id, email: data.email}];
        }
        socketToRoom[socket.id] = data.room;

        socket.join(data.room);
        console.log(`[${socketToRoom[socket.id]}]: ${socket.id} enter`);

        const usersInThisRoom = users[data.room].filter(user => user.id !== socket.id);

        console.log(usersInThisRoom);

        io.sockets.to(socket.id).emit('all_users', usersInThisRoom);
    }); */
});

async function setDeviceStatus(socket, devId, status) {
  const userIdSuccess = await accessToUser.getAccess(devId);
  const userSocketId = await connectedUsersSocketId.get(userIdSuccess);
  socket.to(userSocketId).emit('set_user_status', status);
}
