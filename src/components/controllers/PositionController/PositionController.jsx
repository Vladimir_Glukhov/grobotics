import React, { useCallback } from 'react';
import RoundButton from '~/components/RoundButton.jsx';
import { sendMsgToDevice } from 'app/src/modules/api/api';
import { devicesIds } from 'app/src/modules/constants';

export default ({ userId, deviceId }) => {
    const goPositionStart = useCallback(() => {
        sendMsgToDevice({ devId: devicesIds.ARMS, msg: { id: 'position_start' } }, userId, deviceId);
    }, [userId, deviceId]);

    const goPositionBegin = useCallback(() => {
        sendMsgToDevice({ devId: devicesIds.ARMS, msg: { id: 'position_begin' } }, userId, deviceId);
    }, [userId, deviceId]);

    return <div className="buttons-group-position">
        <RoundButton img="fas fa-lg fa-level-up-alt" className="" onMouseDown={goPositionStart} />
        <RoundButton img="fas fa-lg fa-level-down-alt" className="" onMouseDown={goPositionBegin} />
    </div>;
};