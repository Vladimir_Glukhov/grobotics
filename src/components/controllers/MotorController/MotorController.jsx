import React from 'react';
import { MoverController }  from './MoverController.jsx';

export default ({ deviceId, userId }) => {
    return (
        <div className="controllers">
            <MoverController deviceId={deviceId} userId={userId}/>
        </div>);
};