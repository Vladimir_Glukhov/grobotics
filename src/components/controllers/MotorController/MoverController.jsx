import React, { useState } from 'react';

import Mover from 'app/src/components/common/Mover/Mover.jsx';
import {
    MOTOR_1_STOP_ID,
    MOTOR_1_UP_ID,
    MOTOR_1_DOWN_ID,
    MOTOR_2_STOP_ID,
    MOTOR_2_UP_ID,
    MOTOR_2_DOWN_ID
} from 'app/src/modules/constants';
import { sendMsgToDevice } from 'app/src/modules/api/api';

import { devicesIds } from 'app/src/modules/constants';
import { debounce } from 'app/src/utils/utils';

import './styles.scss';

export const MoverController = ({ deviceId, userId }) => {
    const [powers, setPowers] = useState({ x: 0, y: 0 });
    const motorUp = debounce((pwrLeft, pwrRight) => {
        sendMsgToDevice({ devId: devicesIds.MOT, msg: { id: MOTOR_1_UP_ID, power: Math.ceil(pwrLeft) } }, userId, deviceId);
        sendMsgToDevice({ devId: devicesIds.MOT, msg: { id: MOTOR_2_UP_ID, power: Math.ceil(pwrRight) } }, userId, deviceId);
    });

    const motorDown = debounce((pwrLeft, pwrRight) => {
        sendMsgToDevice({ devId: devicesIds.MOT, msg: { id: MOTOR_1_DOWN_ID, power: Math.ceil(pwrLeft) } }, userId, deviceId);
        sendMsgToDevice({ devId: devicesIds.MOT, msg: { id: MOTOR_2_DOWN_ID, power: Math.ceil(pwrRight) } }, userId, deviceId);
    });

    const motorStop = () => {
        setPowers({ x: 0, y: 0 });
        sendMsgToDevice({ devId: devicesIds.MOT, msg: { id: MOTOR_1_STOP_ID, power: 0 } }, userId, deviceId);
        sendMsgToDevice({ devId: devicesIds.MOT, msg: { id: MOTOR_2_STOP_ID, power: 0 } }, userId, deviceId);
    };

    const motorLeft = debounce((pwr) => {
        const power = Math.ceil(pwr);
        sendMsgToDevice({ devId: devicesIds.MOT, msg: { id: MOTOR_2_UP_ID, power } }, userId, deviceId);
        sendMsgToDevice({ devId: devicesIds.MOT, msg: { id: MOTOR_1_DOWN_ID, power } }, userId, deviceId);
    });

    const motorRight = debounce((pwr) => {
        const power = Math.ceil(pwr);
        sendMsgToDevice({ devId: devicesIds.MOT, msg: { id: MOTOR_2_DOWN_ID, power } }, userId, deviceId);
        sendMsgToDevice({ devId: devicesIds.MOT, msg: { id: MOTOR_1_UP_ID, power } }, userId, deviceId);
    });

    const onTriggerMove = (pX, pY, x, y, width, height) => {
        const absX = Math.abs(width / 2 - x);
        const absY = Math.abs(height / 2 - y);
        if ((pX * pX + pY * pY) > 100) {
            if (absY > 15) {
                if (y < height / 2) {
                    if (absX > 15) {
                        if (x > width / 2) {
                            setPowers({ x: pY, y: 100 - pX });
                            motorUp(pY, 100 - pX);
                        } else {
                            setPowers({ x: 100 - pX, y: pY });
                            motorUp(100 - pX, pY);
                        }
                    } else {
                        setPowers({ x: pY, y: pY });
                        motorUp(pY, pY);
                    }
                } else if (y > height / 2) {
                    if (absX > 15) {
                        if (x > width / 2) {
                            setPowers({ x: -pY, y: -(100 - pX) });
                            motorDown(pY, 100 - pX);
                        } else {
                            setPowers({ x: -(100 - pX), y: -pY });
                            motorDown(100 - pX, pY);
                        }
                    } else {
                        setPowers({ x: -pY, y: -pY });
                        motorDown(pY, pY);
                    }
                }
            } else if (absX > 15) {
                if (x < width / 2) {
                    setPowers({ x: -pX, y: pX });
                    motorLeft && motorLeft(pX);
                } else if (x > width / 2) {
                    setPowers({ x: pX, y: -pX });
                    motorRight && motorRight(pX);
                }
            }
        }
    };
    return (
        <Mover
            onTriggerMove={onTriggerMove}
            onTriggerStop={motorStop}
        />
    );
};