import React, { useCallback } from 'react';
import Button from '../../Button.jsx';
import {
    MOTOR_1_STOP_ID,
    MOTOR_1_UP_ID,
    MOTOR_1_DOWN_ID,
    MOTOR_2_STOP_ID,
    MOTOR_2_UP_ID,
    MOTOR_2_DOWN_ID
} from '../../../modules/constants';

export const LinealController = ({ deviceId, userId }) => {
    const stopMotor = useCallback((id) => () => sendMsgToDevice({ devId: devicesIds.MOT, msg: { id } }, userId, deviceId), [userId, deviceId]);
    const motorMove = useCallback((id) => (power) => sendMsgToDevice({ devId: devicesIds.MOT, msg: { id, power } }, userId, deviceId), [userId, deviceId]);

    return (
        <div className="simple-controller">
            <Button
                id="btn_1"
                callbackStop={stopMotor(MOTOR_1_STOP_ID)}
                callbackUp={motorMove(MOTOR_1_UP_ID)}
                callbackDown={motorMove(MOTOR_1_DOWN_ID)} />
            <Button
                id="btn_2"
                callbackStop={stopMotor(MOTOR_2_STOP_ID)}
                callbackUp={motorMove(MOTOR_2_UP_ID)}
                callbackDown={motorMove(MOTOR_2_DOWN_ID)} />
        </div>);
};