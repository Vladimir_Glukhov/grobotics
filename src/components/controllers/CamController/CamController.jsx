import React, { useState } from 'react';
import RoundButton from '~/components/RoundButton.jsx';
import { sendMsgToDevice } from 'app/src/modules/api/api';
import { devicesIds } from 'app/src/modules/constants';
import Mover from 'app/src/components/common/Mover/Mover.jsx';
import './styles.scss';

export default ({ deviceId, userId, showControlls = true, groupClassName }) => {
    const [showMover, setShowMover] = useState(false);
    const turnLeftSlow = () => sendMsgToDevice({ devId: devicesIds.SOCKET_CAMERA, msg: { id: 'turn', desc: 'left', reg: 'short' } }, userId, deviceId);
    const turnLeft = () => sendMsgToDevice({ devId: devicesIds.SOCKET_CAMERA, msg: { id: 'turn', desc: 'left', reg: 'full' } }, userId, deviceId);
    const turnRightSlow = () => sendMsgToDevice({ devId: devicesIds.SOCKET_CAMERA, msg: { id: 'turn', desc: 'right', reg: 'short' } }, userId, deviceId);
    const turnRight = () => sendMsgToDevice({ devId: devicesIds.SOCKET_CAMERA, msg: { id: 'turn', desc: 'right', reg: 'full' } }, userId, deviceId);

    const turnOff = () => {
        sendMsgToDevice({ devId: devicesIds.SOCKET_CAMERA, msg: { id: 'turn_off' } }, userId, deviceId);
    };

    const onReset = () => sendMsgToDevice({ devId: devicesIds.SOCKET_CAMERA, msg: { id: 'reset_position' } }, userId, deviceId);
    const onTakeImage = () => sendMsgToDevice({ devId: devicesIds.SOCKET_CAMERA, msg: { id: 'take_image' } }, userId, deviceId);

    const onTriggerMove = (pX, pY, x, y, width, height) => {
        const absX = Math.abs(width / 2 - x);
        if ((pX * pX + pY * pY) > 100) {
            if (absX > 15) {
                if (x < width / 2) {
                    if (x > width / 4) turnLeftSlow();
                    if (x < width / 4) turnLeft();
                } else if (x > width / 2) {
                    if (x < width * 3 / 4) turnRightSlow();
                    if (x > width * 3 / 4) turnRight();
                }
            }
        }
    };

    return (<>
        <div className={`d-flex cam-group ${groupClassName}`}>
            {showControlls && <>
                {showMover && <Mover onTriggerMove={onTriggerMove} onTriggerStop={turnOff} />}
                <RoundButton img="fas fa-lg fa-crosshairs" className="cam-group_reset-button" onMouseDown={onReset} />
                <RoundButton img="fas fa-lg fa-chevron-left" className="cam-group_cam-turn-left-button" onMouseDown={turnLeft} onMouseUp={turnOff}/>
                <RoundButton img="fas fa-lg fa-chevron-right" className="cam-group_cam-turn-right-button" onMouseDown={turnRight} onMouseUp={turnOff}/>
            </>}
            <RoundButton img="fas fa-lg fa-camera" className="cam-group_take-image-button" onMouseDown={onReset} />
        </div></>);
};