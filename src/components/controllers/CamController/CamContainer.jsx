import React, { memo, useCallback, useState, useRef, useEffect, useContext, useMemo } from 'react';
import { RobotContext } from 'app/src/modules/context';
import { sendMsgToDevice } from 'app/src/modules/api/api';
import { devicesIds } from 'app/src/modules/constants';
import './styles.scss';

export default memo(({ isOpen, remoteVideoRef, userId, deviceId }) => {
    //const msg = useContext(RobotContext);

    useEffect(() => {
        if (isOpen) {
            //sendMsgToDevice({ devId: devicesIds.SOCKET_CAMERA, msg: { id: 'start' } }, userId, deviceId);
        } else {
            //stop();
            //sendMsgToDevice({ devId: devicesIds.CAM, msg: { id: 'stop' } }, userId, deviceId);
        }
    }, [isOpen]);

    const getRef = useCallback((ref) => ref && (ref.srcObject = remoteVideoRef.current), [remoteVideoRef]);
    console.log(remoteVideoRef);
    return (
    <div className="cam-container">
        <div className="cam">
            <div className="cam-content">
                <video
                    className="remote-video"
                    autoPlay
                    muted
                    playsInline
                    ref={(ref) => ref && (ref.srcObject = remoteVideoRef.current)}
                />
            </div>
        </div>
    </div>
    );    
});
