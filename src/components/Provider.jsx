import React, { useState, useEffect } from 'react';
import { UserContext } from '../modules/context.js';
import { getUserDescription } from '../modules/api/api';

export default ({ children }) => {
    const [description, setDescription] = useState({})
    useEffect(() => {
        const login = localStorage.getItem('login');
        if (login) {
            getUserDescription(login).then(res => {
                const { robots, controllers } = res;//
                setDescription({ robots, controllers });//
            })
        }
    }, [])

    return <UserContext.Provider value={description}>{children}</UserContext.Provider>

}