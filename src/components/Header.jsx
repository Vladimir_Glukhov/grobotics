import React, { useState } from 'react';
import { Link } from "react-router-dom";

import { isMobileDevice } from '../utils/utils';

const Menu = ({ menuItems, isMobile }) => {
    const [status, setStatus] = useState(false);
    const [item, setItem] = useState(menuItems[0]);
    const onToggle = () => setStatus(!status);

    return (<>
        {isMobile && <span></span>}
        <ul className="app-menu-list" onClick={onToggle}>
            {menuItems.map((menuItem, i) => {
                const { title, link, className, icon } = menuItem;
                const isSelected = menuItem === item;

                return (
                    <li
                        onClick={() => setItem(menuItem)}
                        key={'title' + i}
                        className={`app-menu-item ${className || ''} ${isSelected ? 'app-menu-item-selected' : ''}`}>
                        <Link to={link}>
                            <span className="app-menu-item-icon"></span>{isMobile && !isSelected ? title[0] : title}
                        </Link>
                    </li>);
            })}
        </ul></>);
};

const MainMenu = ({ isMobile }) => {
    const mainItems = [
        { title: 'Home', link: '/', icon: 'home.png', className: 'm-l-auto' },
        { title: 'Robots', link: '/robots', icon: 'robots.png' },
        { title: 'Contacts', link: '/contacts', icon: 'contacts.png' },
        { title: 'RoboLand', link: '/roboland', icon: 'roboland.png', className: 'm-l-auto' }];
    return <Menu menuItems={mainItems} isMobile={isMobile} />
}



export default ({ title }) => {
    const isMobile = isMobileDevice();
    console.log('isMobile', isMobile);
    return (
        <header className="app-header">
            <MainMenu isMobile={isMobile} />
        </header>);
};