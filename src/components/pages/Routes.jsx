import React, { Suspense } from 'react';
import { Route, Switch } from "react-router-dom";
import { APP_ROUTES, ROBOLAND_ROUTES } from 'app/src/modules/constants'
import Controllers from './Controllers/Controllers.jsx';
import Robots from './Robots/Robots.jsx';
import RobotsProvider from '../Provider.jsx';

const RoboLand = React.lazy(() => import('./RoboLand/RoboLand.jsx'));

export const RobolandRoutes = () => (
    <RobotsProvider>
        <Switch>
            <Route path={ROBOLAND_ROUTES.robots}>
                <Robots />
            </Route>
            <Route path={`${ROBOLAND_ROUTES.controllers}/:robotId`}>
                <Controllers />
            </Route>
            <Route exact path={APP_ROUTES.roboland}>
                <Suspense fallback={'loading'}>
                    <RoboLand />
                </Suspense>
            </Route>
        </Switch>
    </RobotsProvider>
);