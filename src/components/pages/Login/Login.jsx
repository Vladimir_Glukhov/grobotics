import React, { useState, useEffect, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { LabelInput, SimpleButton } from '../../common';
import { APP_ROUTES } from 'app/src/modules/constants';
import { logIn } from '../../../modules/api/auth';

import './styles.scss';

export default () => {
    const history = useHistory();
    const [status, setStatus] = useState(false);

    const logInSuccess = useCallback((json) => {
        const t = json.token;
        if (t) {
            localStorage.setItem('token', t);
        }        
    }, []);

    useEffect(() => {
        const t = localStorage.getItem('token');
        t && history.push(APP_ROUTES.roboland);
    }, []);

    const onSubmit = (e) => {
            e.preventDefault();
            const  { elements }= e.target;
            const login = elements.login;
            const password = elements.password;//to-do change this to hash
                        
            if (login.value && password.value) {
                setStatus(true);
                logIn(login.value, password.value)
                .then((response) => {
                    if (response.ok) {
                        response.json().then(json => {
                            localStorage.setItem('login', login.value);
                            logInSuccess && logInSuccess(json);
                            history.push(APP_ROUTES.roboland);
                        });
                    } else {
                        
                    }
                })
                .catch(e=> console.log(e))
                .finally(() => setStatus(false));
            }
    };
    
    return <div className={`loginPage main-bg`}>
        <div className="login-page-container">
            <form onSubmit={onSubmit}>
                <LabelInput name="login" label="Login:"/>
                <LabelInput name="password" label="Password:" type="password"/>
                <SimpleButton type="submit">GO</SimpleButton>
            </form>
        </div>
    </div>;
};