import React, { useState, useEffect, useContext } from 'react';
import { useParams } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import { RobotContext } from '../../../modules/context.js';
import { handleRobotMsg, connectUserAndDevice } from '../../../modules/api/api';
import { RideRobotController } from '../../robotControllers';
import { StepRobotController } from '../../robotControllers';
import { ControllerWrapper } from '../../ControllerWrapper/ControllerWrapper.jsx';
import { UserContext } from '../../../modules/context';

const robotControllerMap = {
    ['DEVICE_ID']: RideRobotController,
    ['DEVICE_ID_STEP']: StepRobotController
};

export default () => {
    const [msg, setMsg] = useState({});
    const [valid, setValid] = useState(false);
    const [login, setLogin] = useState(null);
    const description = useContext(UserContext);
    let { robotId } = useParams();
    const history = useHistory();

    useEffect(() => {
        if (description.robots) {
            if (!description.robots.ids.includes(robotId)) {
                //Lets redirect if wrong robot id
                history.push('/roboland/robots');
            } else {
                setValid(true);
                const login = localStorage.getItem('login');
                login && setLogin(login);
            }
        }
    }, [robotId, description]);

    useEffect(() => {
        handleRobotMsg((msg) => setMsg(msg));
    }, []);

    useEffect(() => {
        console.log('init io', login);
        login && connectUserAndDevice(login, robotId);
        return () => {
            console.log('close io');
            //disableUser();
        };
    }, [login, robotId]);

    const RobotController = robotControllerMap[robotId];

    return (
        <RobotContext.Provider value={msg}>
            <div className="container-body">
            {valid && login && <ControllerWrapper deviceId={robotId} userId={login}>
                <RobotController deviceId={robotId} userId={login}/>
            </ControllerWrapper>}
            </div>
        </RobotContext.Provider>);
};