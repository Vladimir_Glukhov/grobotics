import React from 'react';
import Header from '../../Header.jsx';

import './styles.scss';

export default ({ children }) => {
    return (
        <>
            <Header />
            {children}
        </>);
};