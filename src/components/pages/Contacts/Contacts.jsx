import React from 'react';

import './styles.scss';

export default function () {
  return (
    <div className="main-bg">
      <div className="contacts-page-container">
        <a className="" href="mailto:grobotics@.....com">
          <i className="fas fa-envelope title-text title-text-small email">
            <span>{' '}</span>
          </i>
        </a>
        <span className="title-text title-text-small">Main developer Vladimir Glukhov</span>
      </div>
    </div>
  );
}
