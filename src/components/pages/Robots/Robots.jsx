import React, { useEffect, useContext } from "react";
import RobotInfo from "../components/RobotInfo/RobotInfo.jsx";
import { UserContext } from "../../../modules/context";

import './styles.scss';

export default () => {
    const { robots } = useContext(UserContext);

    return (
        <div className="robots-page main-bg">
            <div>
            {!!robots && robots.ids.map(id => <RobotInfo id={id} type={robots[id].type} />)}
            </div>
        </div>
    );
};