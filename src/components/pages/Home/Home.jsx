import React from 'react';
import { Link } from "react-router-dom";

import './styles.scss';

export default () => {
    return (
        <div className="main-bg">
            <div>
                <div className="title-img robo1" />
                <span className="title-text title-text-small title-top">
                    We are developing robots <br />
                    Easy to use, easy to create <br />
                    Develop your robot as you wish <br />
                    Create your code <br />
                    ...and create future
                </span>
            </div>
            <p className="title-text"> Robots are our Future </p>
            <div className="container">
                <div className="title-img robo2 m-l-auto" />
                <span className="title-text title-text-small title-left">
                    One day <br />
                    Robots will be smarter that now <br />
                    And will have AI <br />
                    In future we will have new pets - robots <br />
                    ...
                </span>
            </div>
            <div className="container">
                <div className="title-img robo3" />
                <span className="title-text title-text-small title-right">
                    <Link to="/contacts">Contact us for information about our robots</Link>
                    <Link to="/robots"> ... or look at our projects</Link>
                </span>
            </div>
        </div>);
};