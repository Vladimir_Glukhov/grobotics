import React, { memo, useEffect, useState } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import { getDeviceDescription } from '../../../../modules/api/api';
import Led from '../../../common/Led.jsx';
import { devicesDescriptions, robotsDescriptions } from '../../../../modules/constants';

import './styles.scss';

import ride_bg from 'app/src/img/type1.JPG';
import step_bg from 'app/src/img/type2.JPG';

const imgMap = {
    'ride': ride_bg,
    'step':  step_bg
};

export default memo(({ id, type }) => {
    const [devices, setDevices] = useState(null);
    const [error, setError] = useState(null);
    const match = useRouteMatch();
    console.log(devices);
    useEffect(() => {
        const token = localStorage.getItem('token');
        if (token) {
            getDeviceDescription(id, { token })
                .then((res) => {
                    res.json && res.json().then((desc) => setDevices(desc.devices), (e) => setError(e));
                })
                .catch((e) => setError(e));
        }
    }, [id]);

    return (
        <div className="robot-container robot-ride">
            <div className={`robot-container-img ${imgMap[type]}`} style={{ backgroundImage: `url(${imgMap[type]})`}}/>
            <div className="robot-led-container">
                <Led status={devices?.length > 0} />
                <span className="robot-led-container-power-level">90%</span>
            </div>
            <aside className="button-block-container">
                <Link className="link-button" to={`/roboland/controllers/${id}`}>Control</Link>
                <Link className="link-button" to={`/roboland/setting/${id}`}>Description</Link>
                <Link className="link-button">Map</Link>
                <button className="link-button">Delete</button>
            </aside>
            
        </div>
    );
});