import React from 'react';
import './styles.scss';

export default ({ img, title, description, children }) => {

    return (
        <div className="info-container">
            <span className="info-container-title">{title}</span>
            <div className="info-container-img" style={{ backgroundImage: `url(${img})` }} />
            <span className="info-container-description">{description}</span>
            {children}
        </div>
    );
}