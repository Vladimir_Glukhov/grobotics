import React, { useContext } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

import robots_img from 'app/src/img/robot1.jpg';
import controllers_img from 'app/src/img/robot2.jpg';
import profile_img from 'app/src/img/robot2.jpg';
import { UserContext } from '../../../modules/context';
import InfoContainer from '../components/InfoContainer/InfoContainer.jsx';

import './styles.scss';

export default function () {
  const { controllers } = useContext(UserContext);
  const match = useRouteMatch();

  return (
    <div className="roboland-page main-bg">
      <Link to={`${match.path}/robots`} className="link">
        <InfoContainer img={robots_img} title="Robots" />
      </Link>
      <Link to={`${match.path}/controllers`} className="link" disabled={!controllers}>
        <InfoContainer img={controllers_img} title="Controllers" />
      </Link>
      <Link to={`${match.path}/profile`} className="link">
        <InfoContainer img={profile_img} title="My Profile" />
      </Link>
    </div>
  );
}
