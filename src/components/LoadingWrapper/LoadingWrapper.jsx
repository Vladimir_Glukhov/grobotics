import React, { memo, useCallback, useState, useRef, useEffect, useContext, useMemo } from 'react';
import './styles.scss';

export const LoadingWrapper = memo(({ children, progress = 0, onStart, onCancel }) => {
    const [pauseLoading, setPause] = useState(true);
    const [started, setStarted] = useState(false);

    const isLoading = progress > 0;

    const handleClick = useCallback(() => {
        if (!isLoading) {
            setStarted(true);
            onStart && onStart();
        } else {
            setStarted(false);
            onCancel && onCancel();
        }
    }, [onStart, onCancel, isLoading]);

    useEffect(() => {
        if (progress === 0) setPause(true);
        if (progress === 100) {
            setTimeout(() => setPause(false), 200);
        }
    }, [progress]);

    const message = isLoading ? 'Disconnect' : 'Connect';
    console.log(progress);
    return (progress <= 100 && pauseLoading ? 
        <div className='loadingWrapper'>
            <button className={`loadingWrapper_button ${started ? 'loadingWrapper_button_loading' : 'loadingWrapper_button_preload'}`} onClick={handleClick}>{message}</button>
            {started && <div className="loadingWrapper_progressBar"><div style={{ width: `${progress}%`}} className="loadingWrapper_progressBar_inner"/></div>}
        </div> : children);
});