import React, { useState, useCallback } from 'react';
import CamController from '../controllers/CamController/CamController.jsx';
import CamContainer from '../controllers/CamController/CamContainer.jsx';
import PositionController from '../controllers/PositionController/PositionController.jsx';
import RoundButton from '../RoundButton.jsx';
import InfoPanel from 'app/src/components/informationsPanels/InfoPanelStepRobot/InfoPanelStepRobot.jsx';

import { sendMsgToDevice } from 'app/src/modules/api/api';
import { devicesIds } from 'app/src/modules/constants';

export const StepRobotController = ({ deviceId, userId }) => {
    const [isOpen, setOpen] = useState(false);
    const onToggle = () => setOpen(!isOpen);

    const stopPosition = useCallback(() => {
        sendMsgToDevice({ devId: devicesIds.ARMS, msg: { id: 'position_stop' } }, userId, deviceId);
    }, [userId, deviceId]);

    const goStepUp = useCallback(() => {
        sendMsgToDevice({ devId: devicesIds.ARMS, msg: { id: 'go_step_up' } }, userId, deviceId);
    }, [userId, deviceId]);

    const goStepDown = useCallback(() => {
        sendMsgToDevice({ devId: devicesIds.ARMS, msg: { id: 'go_step_down' } }, userId, deviceId);
    }, [userId, deviceId]);
    
    return (
        <div>
            <div className="d-flex d-f-space-between">
                <CamContainer isOpen={isOpen} deviceId={deviceId} userId={userId} />
                <CamController isOpen={isOpen} deviceId={deviceId} userId={userId} showControlls={false}/>
                <InfoPanel />
                <div className="position-controller-block">
                    <PositionController deviceId={deviceId} userId={userId} />
                </div>
                <RoundButton className="open-camera_button" onClick={onToggle} img={isOpen ? 'fas fa-lg fa-video' : 'fas fa-lg fa-video-slash'} />
                <RoundButton className="stop-operation_button" onClick={stopPosition} img="fas fa-lg fa-stop" />
                <div className="mover-controller-block">
                    <RoundButton onClick={goStepUp} img="fas fa-lg fa-sort-up" />
                    <RoundButton onClick={goStepDown} img="fas fa-lg fa-sort-down" />
                </div>
            </div>
        </div>
    );
};