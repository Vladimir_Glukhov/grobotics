import React, { useState, memo } from 'react';

import Controller from '../controllers/MotorController/MotorController.jsx';
import CamController from '../controllers/CamController/CamController.jsx';
import CamContainer from '../controllers/CamController/CamContainer.jsx';
import RoundButton from '../RoundButton.jsx';
import InfoPanel from 'app/src/components/informationsPanels/InfoPanel/InfoPanel.jsx';

const controllerTypes = { lineal: 'LINEAL', round: 'ROUND ' };

export const RideRobotController = memo(({ deviceId, userId, remoteVideoRef, status }) => {
    const [isOpen, setOpen] = useState(false);
    const onToggle = () => setOpen(!isOpen);
    console.log('render ride controller');
    return (
        <div>
            <div className="d-flex d-f-space-between">
                {/*<RoundButton onClick={onToggleType} text={text} />*/}
                <RoundButton className="open-camera_button " onClick={onToggle} img={isOpen ? 'fas fa-lg fa-video' : 'fas fa-lg fa-video-slash'} />
            </div>
            <CamContainer isOpen={isOpen} deviceId={deviceId} userId={userId} remoteVideoRef={remoteVideoRef} />
            <InfoPanel status={status} />
            <CamController isOpen={isOpen} deviceId={deviceId} userId={userId} />
            <Controller controllerType={controllerTypes.round} deviceId={deviceId} userId={userId} />
        </div>
    );
});