import React, { useState, memo, useEffect, useCallback, useRef } from 'react';

const Toggler = ({ buttonRef, onStartMove, onEndMove, onTriggerMove }) => {
    const togglerRef = useRef();
    const [coord, setCoord] = useState({ x: 0, y: '50%' })

    const startMove = e => {
        const { top, left } = togglerRef.current.getBoundingClientRect();
        let y = e.clientY;
        let x = e.clientX;
        if (!y) {
            y = Array.from(e.changedTouches).find(el => buttonRef.current.hasChildNodes(el.target)).clientY;
        }
        if (!x) {
            x = Array.from(e.changedTouches).find(el => buttonRef.current.hasChildNodes(el.target)).clientX;
        }
        togglerRef.current.mode = true;
        togglerRef.current.startPos = { x: x - left, y: y - top };
        onStartMove && onStartMove();
    };

    const onMove = useCallback(e => {
        if (togglerRef.current.mode) {
            let y = e.clientY;
            let x = e.clientX;
            if (!y) {
                y = Array.from(e.changedTouches).find(el => buttonRef.current.hasChildNodes(el.target)).clientY;
            }
            if (!x) {
                x = Array.from(e.changedTouches).find(el => buttonRef.current.hasChildNodes(el.target)).clientX;
            }
            const { top, left, height } = buttonRef.current.getBoundingClientRect();
            const { height: togglerHeight } = togglerRef.current.getBoundingClientRect();
            let newTop = y - top - togglerRef.current.startPos.y;
            newTop = newTop > 0 ? (newTop < height - togglerHeight ? newTop + togglerHeight / 2 : height - togglerHeight / 2) : togglerHeight / 2;
            const powerY = Math.abs(((newTop) - height / 2) / ((height - togglerHeight) / 2) * 100);
            setCoord({ y: newTop });
            onTriggerMove && onTriggerMove(x, newTop, powerY);
        }
    }, [togglerRef, buttonRef, onTriggerMove]);

    const endMove = useCallback(e => {
        togglerRef.current.mode = false;
        setCoord({ y: '50%' });
        onEndMove && onEndMove();
    }, [onEndMove]);

    const style = { top: coord.y, left: 0 };
    return (
        <div
            onMouseMove={onMove}
            onTouchMove={onMove}
            onMouseLeave={endMove}
            onMouseDown={startMove}
            onTouchStart={startMove}
            onTouchEnd={endMove}
            onMouseUp={endMove}
            ref={togglerRef}
            className="inner-toggler"
            style={style} />);
}

export default memo(({ id, className, callbackStop, callbackUp, callbackDown }) => {
    const buttonElem = useRef();
    const onTriggerMove = (x, y, p) => {
        const { height } = buttonElem.current.getBoundingClientRect();
        if (y < height / 2) {
            callbackUp && callbackUp(p);
        }
        else callbackDown && callbackDown(p);
    }
    const onMouseMove = e => {
        e.preventDefault();
    };

    useEffect(() => {
        buttonElem.current.addEventListener('mousemove', onMouseMove);
        buttonElem.current.addEventListener('touchmove', onMouseMove);
        return () => {
            buttonElem.current && buttonElem.current.removeEventListener('mousemove', onMouseMove);
            buttonElem.current && buttonElem.current.removeEventListener('touchmove', onMouseMove);
        }
    }, []);

    return (
        <div ref={buttonElem} className="btn-toggler" id={id}>
            <Toggler
                buttonRef={buttonElem}
                onTriggerMove={onTriggerMove}
                onEndMove={callbackStop}/>
        </div>)
});