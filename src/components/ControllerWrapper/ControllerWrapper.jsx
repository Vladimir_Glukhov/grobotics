import React, { useCallback, useState, memo } from 'react';
import { ConnectionWrapper } from '../ConnectionWrapper/ConnectionWrapper.jsx';
import { LoadingWrapper } from '../LoadingWrapper/LoadingWrapper.jsx';
//import './styles.scss';

export const ControllerWrapper = memo(({ deviceId, userId, children }) => {
    const [isOpenConnection, setIsOpenConnection] = useState(false);
    const [progress, setProgress] = useState(0);

    const onStartConnection = useCallback(() => {
        setIsOpenConnection(true);
    }, []);

    const onCancelConnection = useCallback(() => {
        setIsOpenConnection(false);
    }, []);

    const onProgress = useCallback((p) =>{
        setProgress(p);
    }, []);

    const RenderComponent = useCallback(props => (
                    <LoadingWrapper progress={progress} onStart={onStartConnection} onCancel={onCancelConnection}>
                        {React.cloneElement(children, { ...props })}
                    </LoadingWrapper>),
    [progress, children, onStartConnection, onCancelConnection]);
    
    return <ConnectionWrapper
                deviceId={deviceId}
                userId={userId}
                isOpenConnection={isOpenConnection}
                onProgress={onProgress}
                render={RenderComponent}
            />;
});