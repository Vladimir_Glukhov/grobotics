import React, { useContext, useState, useEffect } from 'react';
import RobotStatusDisplay from './RobotStatusDisplay/RobotStatusDisplay.jsx';
import { ProgressBar } from '../../common/ProgressBar';
import { Led } from '../../common/Led';
import { RobotContext } from '../../../modules/context';
import { devicesIds } from '../../../modules/constants';
import './styles.scss';

export default ({ status }) => {
    const msg = useContext(RobotContext);
    const distMsg = msg[devicesIds.MSDIST];
    const cam = msg[devicesIds.CAM] || {};
    const pwr = msg[devicesIds.PWR] || {};


    return (
        <div className="info-panel">
            <div className="progress-bar-content">
                <Led className="led-status" status={status} />
                <ProgressBar className="progress-bar-container" level={pwr.main} w={70} h={70} disabled={!status} />
                <ProgressBar className="progress-bar-container progress-bar-inner" level={pwr.inner} w={60} h={60} disabled={!status} />
            </div>
            <RobotStatusDisplay
                resolution={cam.resolution}
                distance={distMsg?.distance}
                camAngle={(cam.stepCount || 0) * 360 / (64 * 8)}
                faces={cam.faces} />
        </div>
    );
};