import React from 'react';

export default () => {
    return <svg className="level-display level-display-svg" xmlns="http://www.w3.org/2000/svg" width="800.000" height="600.000" viewBox="0.0 0.0 800.000 600.000" version="1.1">
        <title>Produce by Acme CAD Converter</title>
        <desc>Produce by Acme CAD Converter</desc>
        <polyline points="31.49,165.87 51.97,173.33" strokeWidth="1" />
        <polyline points="13.8,231.9 24.53,233.79" strokeWidth="1" />
        <polyline points="7.84,300 29.63,300" strokeWidth="1" />
        <polyline points="31.49,434.13 51.97,426.67" strokeWidth="1" />
        <polyline points="13.8,368.1 24.53,366.21" strokeWidth="1" />
        <polyline points="768.51,165.87 748.03,173.33" strokeWidth="1" />
        <polyline points="786.2,231.9 775.47,233.79" strokeWidth="1" />
        <polyline points="792.16,300 770.37,300" strokeWidth="1" />
        <polyline points="768.51,434.13 748.03,426.67" strokeWidth="1" />
        <polyline points="786.2,368.1 775.47,366.21" strokeWidth="1" />
        <polyline points="31.49,165.87 27.42,177.63 23.73,189.52 20.41,201.51 17.48,213.6 14.93,225.78 12.77,238.04 11,250.36 9.62,262.72 8.63,275.13 8.04,287.56 7.84,300 8.04,312.44 8.63,324.87 9.62,337.28 11,349.64 12.77,361.96 14.93,374.22 17.48,386.4 20.41,398.49 23.73,410.48 27.42,422.37 31.49,434.13" strokeWidth="1" />
        <polyline points="768.51,434.13 772.58,422.37 776.27,410.48 779.59,398.49 782.52,386.4 785.07,374.22 787.23,361.96 789,349.64 790.38,337.28 791.37,324.87 791.96,312.44 792.16,300 791.96,287.56 791.37,275.13 790.38,262.72 789,250.36 787.23,238.04 785.07,225.78 782.52,213.6 779.59,201.51 776.27,189.52 772.58,177.63 768.51,165.87" strokeWidth="1" />
    </svg>

}