import React, { useState, useEffect, useCallback } from 'react';
import LevelDisplayH from './LevelDisplayH.jsx';
import LevelDisplayV from './LevelDisplayV.jsx';

import './styles.scss';

export default ({ xDeg = 0, yDeg = 0 }) => {
    const styleLine = {
        transform: `translateX(-50%) rotateZ(${xDeg}deg)`
    };
    const styleVerticalLine = {
        transform: `translateY(${yDeg*6}px)`
    };
    return (<div className="info-panel-container">
        <div className="level-panel-grad-container">
            <span className="level-panel-text">{Math.ceil(xDeg)}</span>
            <LevelDisplayH />            
        </div>
        <div className="level-display-container-v">
                <LevelDisplayV style={styleVerticalLine}/>
        </div>
        <div className="level-line-horizontal" style={styleLine} />
    </div>);
}