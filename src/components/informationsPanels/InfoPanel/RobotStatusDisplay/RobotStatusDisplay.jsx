import React from 'react';

import RobotHead from './Head.jsx';
import RobotCase from './Case.jsx';

import './styles.scss';

export default ({ distance, camAngle }) => {
    const { d1, d2 } = distance || {};

    return (<>
        <div className="robot-status-display">
            <RobotCase />
            <RobotHead angle={camAngle} />
        </div>
    </>);
}