import React, { useContext, useState, useEffect } from 'react';
import { ProgressBar } from '../../common/ProgressBar';
import { Led } from '../../common/Led';
import { RobotContext } from '../../../modules/context';
import { devicesIds } from '../../../modules/constants';
import { initUser } from '../../../modules/api/api';

export default () => {
    const msg = useContext(RobotContext);
    const [status, setLedStatus] = useState(false);
    const distMsg = msg[devicesIds.MSDIST];
    const pwr = msg[devicesIds.PWR] || {};
    //const [pwr, setPwr] = useState({ inner: 100, main: 100});
    
    useEffect(() => {
        initUser([setLedStatus]);
        //setInterval(() => {
            //setPwr(({ inner, main})=> ({ inner: inner > 0 ? inner - 2 : 100, main: main > 0 ? main - 3 : 100 }))
        //}, 2000);
    }, []);

    return (
        <div className="info-panel">
            <div className="progress-bar-content">
                <Led className="led-status" status={status} />
                <ProgressBar className="progress-bar-container" level={pwr.main} w={70} h={70} disabled={!status} />
                <ProgressBar className="progress-bar-container progress-bar-inner" level={pwr.inner} w={60} h={60} disabled={!status} />
            </div>
        </div>
    );
};