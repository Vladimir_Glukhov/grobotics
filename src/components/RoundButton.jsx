import React, { useRef, useEffect } from 'react';

export default ({ className, onClick, onMouseDown, onMouseUp, text, img }) => {
    const onMouseMove = e => {
        e.preventDefault();
    };

    useEffect(() => {
        buttonElem.current.addEventListener('mousemove', onMouseMove);
        buttonElem.current.addEventListener('touchmove', onMouseMove);
        return () => {
            buttonElem.current && buttonElem.current.removeEventListener('mousemove', onMouseMove);
            buttonElem.current && buttonElem.current.removeEventListener('touchmove', onMouseMove);
        };
    }, []);

    const onUp = (e) => {
        e.preventDefault();
    };

    const buttonElem = useRef();

    return (
        <button
            ref={buttonElem}
            className={`btn-rnd ${className ? className : ''}`}
            onClick={onClick}
            onMouseDown={onMouseDown}
            onTouchStart={onMouseDown}
            onContextMenu={onUp}
            onMouseUp={onMouseUp}
            onTouchEnd={onMouseUp}
            onSelect={e=>e.preventDefault()}
        >
            {img ? <i class={img}></i> : text}
        </button>);
};