import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { APP_ROUTES } from 'app/src/modules/constants';
import { getUser } from 'app/src/modules/api/auth';

export default function AuthWrapper({ children }) {
  const [isValid, setIsValid] = useState(false);
  const history = useHistory();

  const incorrectToken = () => {
    setIsValid(false);
    localStorage.removeItem('token');
    history.push(APP_ROUTES.login);
  };

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      getUser(token).then((response) => {
        if (response.ok) {
          response.json().then((json) => {
            if (json.auth) {
              setIsValid(true);
            } else {
              incorrectToken();
            }
          });
        } else {
          incorrectToken();
        }
      });
    } else {
      incorrectToken();
    }
  }, []);

  return isValid && children;
}
