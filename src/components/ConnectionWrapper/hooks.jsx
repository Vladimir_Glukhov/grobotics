import { useRef, useCallback, useEffect } from 'react';

const RTCPeerConnection = window.RTCPeerConnection || /*window.mozRTCPeerConnection ||*/ window.webkitRTCPeerConnection;
const RTCSessionDescription = /*window.mozRTCSessionDescription ||*/ window.RTCSessionDescription;
const RTCIceCandidate = /*window.mozRTCIceCandidate ||*/ window.RTCIceCandidate;

const pcOptions = {};
const mediaConstraints = {
    optional: [],
    mandatory: {
        OfferToReceiveAudio: true,
        OfferToReceiveVideo: true
    }
};

export const usePeerConnection = (pcConfig, { onIceCandidate, onTrack, onAddStream, onRemoveStream, onError }) => {
    const pc = useRef(null);
    const remoteDesc = useRef(false);
    const iceCandidates = useRef([]);

    const initPeerConnection = useCallback(() => {
        try {
            const pcConfig_ = pcConfig;        
            const _pc = new RTCPeerConnection(pcConfig_, pcOptions);

            _pc.onicecandidate = (event) => {
                if (event.candidate && event.candidate.candidate) {
                    const candidate = {
                        sdpMLineIndex: event.candidate.sdpMLineIndex,
                        sdpMid: event.candidate.sdpMid,
                        candidate: event.candidate.candidate
                    };
                    const request = {
                        what: "addIceCandidate",
                        data: JSON.stringify(candidate)
                    };
                    onIceCandidate && onIceCandidate(JSON.stringify(request));
                } else {
                    console.log("End of candidates.");
                }
            };

            if ('ontrack' in _pc) {
                _pc.ontrack = (event) => {
                    console.log("Remote track!");
                    onTrack && onTrack(event.streams[0]);
                };
            } else {
                _pc.onaddstream = (event) => {
                    console.log("Remote stream added:", event.stream);
                    onAddStream && onAddStream(event.stream);
                };
            }
            _pc.onremovestream = () => {
                onRemoveStream && onRemoveStream();
            };

            pc.current = _pc;

            //pc.ondatachannel = onDataChannel;

            console.log("peer connection successfully created!");
        } catch (e) {
            onError && onError(e);
        }    

    }, [pcConfig, onIceCandidate, onTrack, onAddStream, onRemoveStream, onError]);

    const addIceCandidates = useCallback(() => {
        console.log('add candidates',  [...iceCandidates.current]);
        iceCandidates.current.forEach((candidate) => {
            pc.current && pc.current.addIceCandidate(candidate).then(
                () => {
                    console.log("IceCandidate added: " + JSON.stringify(candidate));
                },
                (error) => {
                    onError && onError(error);
                }
            );
        });
        iceCandidates.current = [];
    }, []);  

    const onOffer = useCallback((data, onAnswer) => {
        const sessionDesctiption = new RTCSessionDescription(JSON.parse(data));
        console.log('sessionDesctiption', sessionDesctiption);
        pc.current.setRemoteDescription(sessionDesctiption).then(() => {
                    remoteDesc.current = true;
                    addIceCandidates();
                    console.log('onRemoteSdpSucces()');
                    pc.current.createAnswer().then(
                    (sessionDescription) => {
                        console.log('answer created');
                        pc.current.setLocalDescription(sessionDescription);
                        const request = {
                            what: "answer",
                            data: JSON.stringify(sessionDescription)
                        };
                        onAnswer && onAnswer(JSON.stringify(request));
                        console.log(request);
                    },
                    (error) => {
                        onError(error);
                    }, mediaConstraints);
        });
    }, [onError, addIceCandidates]);

    const onAddIceCandidate = useCallback((data) => {
        const elt = JSON.parse(data);
        const candidate = new RTCIceCandidate({sdpMLineIndex: elt.sdpMLineIndex, candidate: elt.candidate});
        iceCandidates.current = [...iceCandidates.current, candidate];
        if (remoteDesc.current) {
            addIceCandidates();
        }
    }, [addIceCandidates]);

    const onAddIceCandidates = useCallback((data) => {
        const candidates = JSON.parse(data);
        for (let i = 0; candidates && i < candidates.length; i++) {
            const elt = candidates[i];
            let candidate = new RTCIceCandidate({sdpMLineIndex: elt.sdpMLineIndex, candidate: elt.candidate});
            iceCandidates.push(candidate);
        }
        if (remoteDesc.current)
            addIceCandidates();
    }, [addIceCandidates]);

    const onClose = useCallback(() => {
        remoteDesc.current = false;
        iceCandidates.current = [];
        if (pc.current) {
            pc.current.close();
            pc.current = null;
        }
    }, []);
    return [pc, initPeerConnection, { onOffer, onClose, onAddIceCandidate, onAddIceCandidates }];
};

export const useWebSocket = () => {
    const ws = useRef(null);

    const createWebSocket = useCallback((url, { onError }) => {
        try {
            ws.current = new WebSocket(url);
        } catch (e) {
            onError && onError(e);
        }
    }, []);

    const initWebsocket = useCallback(({ onOpenWebsocket, onOfferWebsocket, onAddIceCandidate, onAddIceCandidates, onCloseWebSocket, onError, onIceComplete }) => {
        if (!ws.current) return;

        ws.current.onopen = () => {
            onOpenWebsocket && onOpenWebsocket();
        };
        ws.current.onmessage = (evt) => {
            const msg = JSON.parse(evt.data);
            let what;
            let data;
            if (msg.what !== 'undefined') {
                what = msg.what;
                data = msg.data;
            }
            switch (what) {
                case "offer":
                    onOfferWebsocket && onOfferWebsocket(data, (_data) => ws.current.send(_data));
                    break;
                case "answer":
                    break;
                case "message":
                    console.log(msg.data);
                    break;

                case "iceCandidate": // when trickle is enabled
                    if (!msg.data) {
                        console.log("Ice Gathering Complete");
                        onIceComplete && onIceComplete();
                        break;
                    }
                    onAddIceCandidate && onAddIceCandidate(msg.data);
                    break;

                case "iceCandidates": // when trickle ice is not enabled
                    onAddIceCandidates && onAddIceCandidates(msg.data);
                    onIceComplete && onIceComplete();
                    break;
            }
        };
        ws.current.onclose = () => {
            onCloseWebSocket && onCloseWebSocket();
        };

        ws.current.onerror = (e) => {
            onError && onError(e);
            ws.current.close();
        };
    }, []);

    const onIceCandidate = useCallback((data) => {
        ws.current.send(data);
    }, []);

    return [ws, createWebSocket, initWebsocket, { onIceCandidate }];
};

export const useSocket = ({
    userId,
    deviceId,
    handleDeviceMessage,
    sendMsgToDevice,
    removeDeviceListeter
    }) => {
    
    const initWebsocket = useCallback(({
            onOfferWebsocket,
            onAddIceCandidate,
            onAddIceCandidates,
            onIceComplete
            }) => {

    const handleWebsocket = (userId, msg) => {
        const _msg = JSON.parse(msg.msg.data);
        let what;
        let data;
        if (_msg.what !== 'undefined') {
            what = _msg.what;
            data = _msg.data;
        }
        switch (what) {
                case "offer":
                    onOfferWebsocket && onOfferWebsocket(data, (_data) => {
                        const msg = { devId: 'SOCKET_CAMERA', msg: { id: 'socket', data: _data } };
                        sendMsgToDevice(msg, userId, deviceId);
                    });
                    break;
                case "answer":
                    break;
                case "message":
                    console.log(_msg.data);
                    break;

                case "iceCandidate": // when trickle is enabled
                    if (!_msg.data) {
                        console.log("Ice Gathering Complete");
                        onIceComplete && onIceComplete();
                        break;
                    }
                    onAddIceCandidate && onAddIceCandidate(_msg.data);
                    break;

                case "iceCandidates": // when trickle ice is not enabled
                    onAddIceCandidates && onAddIceCandidates(_msg.data);
                    onIceComplete && onIceComplete();
                    break;
            }
    };

        return handleWebsocket;
    }, []);

    const onIceCandidate = useCallback((data) => {
        const msg = { devId: 'SOCKET_CAMERA', msg: { id: 'socket', data } };
        sendMsgToDevice(msg, userId, deviceId);
    }, []);

    return [initWebsocket, { onIceCandidate }];
};