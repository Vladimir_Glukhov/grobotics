/* eslint-disable no-unused-expressions */
/* eslint-disable react/prop-types */
import React, {
  useEffect, useRef, useState, useCallback, memo,
} from 'react';
import {
  initUser,
  handleDeviceMessage,
  removeDeviceListeter,
  sendMsgToDevice,
} from 'app/src/modules/api/api';
import { devicesIds } from 'app/src/modules/constants';
import { usePeerConnection, useSocket } from './hooks.jsx';

const stunUrls = [
  'stun:stun.1und1.de:3478',
  'stun:stun.gmx.net:3478',
  'stun:stun.l.google.com:19302',
  'stun:stun1.l.google.com:19302',
  'stun:stun2.l.google.com:19302',
  'stun:stun3.l.google.com:19302',
  'stun:stun4.l.google.com:19302',
  'stun:23.21.150.121:3478',
  'stun:numb.viagenie.ca:3478',
  'stun:stun.12connect.com:3478',
  'stun:stun.12voip.com:3478',
  'stun:stun.2talk.co.nz:3478',
  'stun:stun.2talk.com:3478',
  'stun:stun.3clogic.com:3478',
  'stun:stun.3cx.com:3478',
  'stun:stun.a-mm.tv:3478',
  'stun:stun.aa.net.uk:3478',
  'stun:stun.acrobits.cz:3478',
  'stun:stun.actionvoip.com:3478',
  'stun:stun.advfn.com:3478',
  'stun:stun.aeta-audio.com:3478',
  'stun:stun.aeta.com:3478',
  'stun:stun.altar.com.pl:3478',
  'stun:stun.annatel.net:3478',
  'stun:stun.antisip.com:3478',
  'stun:stun.arbuz.ru:3478',
  'stun:stun.avigora.fr:3478',
  'stun:stun.awa-shima.com:3478',
  'stun:stun.b2b2c.ca:3478',
  'stun:stun.bahnhof.net:3478',
  'stun:stun.barracuda.com:3478',
  'stun:stun.bluesip.net:3478',
  'stun:stun.bmwgs.cz:3478',
  'stun:stun.botonakis.com:3478',
  'stun:stun.budgetsip.com:3478',
  'stun:stun.cablenet-as.net:3478',
  'stun:stun.callromania.ro:3478',
  'stun:stun.callwithus.com:3478',
  'stun:stun.chathelp.ru:3478',
  'stun:stun.cheapvoip.com:3478',
  'stun:stun.ciktel.com:3478',
  'stun:stun.cloopen.com:3478',
  'stun:stun.comfi.com:3478',
  'stun:stun.commpeak.com:3478',
  'stun:stun.l.google.com:19302',
];

// eslint-disable-next-line import/prefer-default-export
export const ConnectionWrapper = memo(
  ({
    render, userId, deviceId, onProgress, isOpenConnection,
  }) => {
    const [pcConfig, setPcConfig] = useState(null);
    const [progressMessage, setProgressMessage] = useState('');
    const progress = useRef(0);

    const remoteVideoRef = useRef(null);
    const handleRef = useRef();
    const onAddStream = useCallback((stream) => {
      remoteVideoRef.current = stream;
    }, []);

    const onTrack = useCallback((stream) => {
      remoteVideoRef.current = stream;
    }, []);
    const onRemoveStream = useCallback(() => {
      remoteVideoRef.current = null;
    }, []);

    const onError = useCallback((error) => {
      alert(error?.message || error);
      progress.current = 0;
    }, []);

    const [initWebsocket, { onIceCandidate }] = useSocket({
      userId,
      deviceId,
      handleDeviceMessage,
      sendMsgToDevice,
      removeDeviceListeter,
    });

    const [
      pc,
      initPeerConnection,
      {
        onClose, onOffer, onAddIceCandidate, onAddIceCandidates,
      },
    ] = usePeerConnection(pcConfig, {
      onIceCandidate,
      onTrack,
      onAddStream,
      onRemoveStream,
      onError,
    });

    const stop = useCallback(() => {
      console.log('stopped', pc.current);
      if (pc.current) {
        pc.current.close();
        pc.current = null;
      }
    }, [pc]);

    const onOpenWebsocket = useCallback(
      (stream) => {
        initPeerConnection();
        progress.current += 50;
        setProgressMessage('Connected to device');

        if (stream) {
          pc.current.addStream(stream);
        }
        const request = {
          what: 'call',
          options: {
            force_hw_vcodec: true,
            trickle_ice: true,
          },
        };
        const msg = {
          devId: 'SOCKET_CAMERA',
          msg: { id: 'socket', data: JSON.stringify(request) },
        };
        sendMsgToDevice(msg, userId, deviceId);
        console.log(`call(), request=${JSON.stringify(request)}`);
      },
      [pc, initPeerConnection, userId, deviceId],
    );

    const [status, setStatus] = useState(false);

    useEffect(() => {
      setPcConfig({
        iceServers: [
          {
            urls: [...stunUrls, `stun:${location.hostname}:3478`],
          },
        ],
      });
      initUser([setStatus]);
    }, []);

    useEffect(() => {
      console.log('init websocket');
      handleRef.current = initWebsocket({
        onOpenWebsocket,
        onOfferWebsocket: onOffer,
        onAddIceCandidate,
        onAddIceCandidates,
        onCloseWebSocket: onClose,
        onError: (e) => {
          alert(e?.message);
          progress.current = 0;
          pc.current && pc.current.close();
        },
        onIceComplete: () => {
          progress.current += 50;
          setProgressMessage('RTC connection completed');
        },
      });

      handleDeviceMessage(devicesIds.SOCKET_CAMERA, handleRef.current);

      return () => {
        removeDeviceListeter(devicesIds.SOCKET_CAMERA);
      };
    }, [
      onOpenWebsocket,
      onOffer,
      onAddIceCandidate,
      onAddIceCandidates,
      onClose,
    ]);

    useEffect(() => {
      if (status) {
        console.log('init web');
        sendMsgToDevice(
          { devId: devicesIds.SOCKET_CAMERA, msg: { id: 'start' } },
          userId,
          deviceId,
        );
      }

      return () => {
        handleRef.current && stop();
        handleRef.current
          && sendMsgToDevice(
            { devId: devicesIds.SOCKET_CAMERA, msg: { id: 'stop' } },
            userId,
            deviceId,
          );
      };
    }, [userId, deviceId, status]);

    useEffect(() => {
      progress.current = 0;
      setProgressMessage('');
      if (isOpenConnection) {
        onOpenWebsocket();
      } else {
        stop();
      }
    }, [isOpenConnection, onOpenWebsocket, stop]);

    useEffect(() => {
      onProgress && onProgress(progress.current, progressMessage);
    }, [onProgress, progress.current, progressMessage]);

    return <>{render({ remoteVideoRef, status })}</>;
  },
);
