import React from 'react';

import './styles.scss';

export const LabelInput = ({ name, placeholder, label, type = "text" }) => (
    <span className="label-input-container">
        <label className="label-input-label" htmlFor={name}>{label}</label>
        <input className="label-input-input" name={name} type={type} placeholder={placeholder}/>
    </span>
);