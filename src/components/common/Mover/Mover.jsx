import React, { useRef, useState, useEffect, useCallback } from 'react';

import Toggler from '../Toggler/Toggler.jsx';

import './styles.scss';

const Mover = ({ className, onTriggerMove, onTriggerStop }) => {
    const buttonElem = useRef();
    const [animClassName, setAnimClassName] = useState();

    const onReset = useCallback(() => {
        setAnimClassName('');
        onTriggerStop && onTriggerStop();
    }, [onTriggerStop]);

    const onMouseMove = e => {
        e.preventDefault();
    };

    const handleTriggerMove = (x, y, pX, pY) => {
        const { height, width } = buttonElem?.current?.getBoundingClientRect();
        const absX = Math.abs(width / 2 - x);
        const absY = Math.abs(height / 2 - y);
        
        if ((pX * pX + pY * pY) > 100) {
            onTriggerMove(pX, pY, x, y, width, height);
            if (absY > 15) {
                if (y < height / 2) {
                    setAnimClassName('mover-middler-anim-top');
                } else if (y > height / 2) {
                    setAnimClassName('mover-middler-anim-bottom');
                }
            } else if (absX > 15) {
                if (x < width / 2) {
                    setAnimClassName('mover-middler-anim-left');
                } else if (x > width / 2) {
                    setAnimClassName('mover-middler-anim-right');
                }
            }
        }
    };

    useEffect(() => {
        buttonElem.current.addEventListener('mousemove', onMouseMove);
        buttonElem.current.addEventListener('touchmove', onMouseMove);
        return () => {
            buttonElem.current && buttonElem.current.removeEventListener('mousemove', onMouseMove);
            buttonElem.current && buttonElem.current.removeEventListener('touchmove', onMouseMove);
        }
    }, []);

    return (
        <div className="mover" ref={buttonElem}>
            <div className={`mover-middler ${animClassName}`} />
            <Toggler
                className="mover-toggler"
                buttonRef={buttonElem}
                onTriggerMove={handleTriggerMove}
                onEndMove={onReset}
                startX="50%"
                startY="50%" />
        </div>
    );
};

export default Mover;