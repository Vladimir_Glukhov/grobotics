import React from 'react';

import './styles.scss';

export const SimpleButton = ({ onClick, type, children, style }) => {
    return <button className={`simple-button ${style}`}type={type} onClick={onClick} >{children}</button>
}