import React from 'react';

import './styles.scss';

export const Led = ({ className, id, status }) => {
    return <div id={id} className={`${className} led-status-led led-status-${status ? 'on' : 'off'}`} />;
};