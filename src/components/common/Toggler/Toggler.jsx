import React, { useCallback, useRef, useState } from 'react';

const Toggler = ({ className, buttonRef, onStartMove, onEndMove, onTriggerMove, startX = 0, startY = 0 }) => {
    const togglerRef = useRef();
    const [coord, setCoord] = useState({ x: startX, y: startY })

    const startMove = e => {
        const { top, left } = togglerRef.current.getBoundingClientRect();
        let y = e.clientY;
        let x = e.clientX;
        if (!y) {
            y = Array.from(e.changedTouches).find(el => buttonRef.current.hasChildNodes(el.target)).clientY;
        }
        if (!x) {
            x = Array.from(e.changedTouches).find(el => buttonRef.current.hasChildNodes(el.target)).clientX;
        }
        togglerRef.current.mode = true;
        togglerRef.current.startPos = { x: x - left, y: y - top };
        onStartMove && onStartMove();
    };

    const onMove = useCallback(e => {
        if (togglerRef.current.mode) {
            let y = e.clientY;
            let x = e.clientX;

            if (!y) {
                y = Array.from(e.changedTouches).find(el => buttonRef.current.hasChildNodes(el.target)).clientY;
            }
            if (!x) {
                x = Array.from(e.changedTouches).find(el => buttonRef.current.hasChildNodes(el.target)).clientX;
            }

            const { top, left, height, width } = buttonRef.current.getBoundingClientRect();
            const { height: togglerHeight, width: togglerWidth } = togglerRef.current.getBoundingClientRect();
            
            let newTop = y - top - togglerRef.current.startPos.y;
            let newLeft = x - left - togglerRef.current.startPos.x;

            newTop = newTop > 0 ? (newTop < height - togglerHeight ? newTop + togglerHeight / 2 : height - togglerHeight / 2) : togglerHeight / 2;
            newLeft = newLeft > 0 ? (newLeft < width - togglerWidth ? newLeft + togglerWidth / 2 : width - togglerWidth / 2) : togglerWidth / 2;
            
            const powerY = Math.abs(((newTop) - height / 2) / ((height - togglerHeight) / 2) * 100);
            const powerX = Math.abs(((newLeft) - width / 2) / ((width - togglerWidth) / 2) * 100);
            
            setCoord({ x: newLeft, y: newTop });
            onTriggerMove && onTriggerMove(newLeft, newTop, powerX, powerY);
        }
    }, [togglerRef, buttonRef, onTriggerMove]);

    const endMove = useCallback(e => {
        togglerRef.current.mode = false;
        setCoord({ x: startX, y: startY });
        onEndMove && onEndMove();
    }, [onEndMove]);

    const style = { top: coord.y, left: coord.x };

    return (
        <div
            onMouseMove={onMove}
            onTouchMove={onMove}
            onMouseLeave={endMove}
            onMouseDown={startMove}
            onTouchStart={startMove}
            onTouchEnd={endMove}
            onMouseUp={endMove}
            ref={togglerRef}
            className={className}
            style={style} />);
};

export default Toggler;