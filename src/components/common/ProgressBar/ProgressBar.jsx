import React from "react";

import './styles.scss';

export const ProgressBar = ({ className, level = 100, R = 50, w = 100, h = 100, disabled }) => {
    const maxLength = Math.PI * (R - 5);
    const length = Math.ceil(level * maxLength / 50);
    const color = level > 75 ? '#08eb00' : level > 50 ? '#99ef03' : level > 25 ? '#efee03' : '#ef0303';
    const angl = level / 50 * Math.PI;
    const posX = 50 - Math.cos(angl) * (R - 5);
    const posY = 50 - Math.sin(angl) * (R - 5);

    return (
        <div className={`${className} progress-bar`} style={{ width: `${w}px`, height: `${h}px` }}>
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                xlinkHref="http://www.w3.org/1999/xlink" viewBox="0 0 100 100">
                <circle
                    cx={50} cy={50}
                    r={R - 5} stroke={disabled ? 'gray' : '#fffcf96e'}
                    strokeWidth="10"
                    fill="none" />
                {!disabled && <>
                    <circle cx="5" cy={50} r="5" fill={color} />
                    <circle className="progress-bar-trn" cx={posX} cy={posY} r="5" fill={color} />
                    <circle
                        className="progress-bar-trn"
                        cx={50} cy={50}
                        r={R - 5} stroke={color}
                        strokeWidth="10"
                        strokeDasharray={`${length} ${Math.ceil(2 * maxLength - length)}`}
                        strokeDashoffset={Math.ceil(maxLength)}
                        fill="none" />
                </>}
            </svg>
        </div>
    );
}