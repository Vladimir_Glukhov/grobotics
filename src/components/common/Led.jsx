import React from 'react';

export default ({ status }) => <div className={`led-status-led led-status-${status ? 'on' : 'off'}`} />;