import { hot } from 'react-hot-loader/root';
import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import history from './modules/router/history';

import MainLayout from './components/pages/MainLayout/MainLayout.jsx';
import Home from './components/pages/Home/Home.jsx';
import AuthWrapper from './components/AuthWrapper.jsx';
import LoginPage from './components/pages/Login/Login.jsx';
import ContactsPage from './components/pages/Contacts/Contacts.jsx';

import { RobolandRoutes } from './components/pages/Routes.jsx';

function App() {
  return (
    <Router history={history}>
      <MainLayout>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/login">
          <LoginPage />
        </Route>
        <Route exact path="/contacts">
          <ContactsPage />
        </Route>
        <Route path="/roboland">
          <AuthWrapper>
            <RobolandRoutes />
          </AuthWrapper>
        </Route>
      </MainLayout>
    </Router>
  );
}

export default App;
