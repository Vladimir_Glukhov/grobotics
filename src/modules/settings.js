export const USER_ID = 'USER_ID';
export const DEVICE_ID = 'DEVICE_ID';
export const DEVICE_ID_STEP = 'DEVICE_ID_STEP';

// export const serverUrl = 'http://localhost:8080/';
export const serverUrl = 'http://localhost:3000/';
// export const serverUrl = 'http://192.168.1.14:3000/';
export const socketUrl = 'http://192.168.1.14:3030/';
// export const serverUrl = 'https://grobotics.herokuapp.com/';
