import { createContext } from 'react';

export const RobotContext = createContext({});

export const UserContext = createContext({});
