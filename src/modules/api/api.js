import { serverUrl } from '../settings';
import socket from '../socket';

let reqId = 0;
let dateSet = {};

function startRequest({ devId, msg }, userId, deviceId) {
    //if (Date.now() - (dateSet[msg.id] || 0) > 50) {//there is different between prev request
        //dateSet[msg.id] = Date.now();
    reqId++;
    socket.emit('send', userId, deviceId, { devId, msg });
    //}
}

export const initUser = (callbacks) => {
    //all listeners there
    console.log('init listeners');
    socket.on('set_user_status', status => {
        console.log('change status', status);
        callbacks && callbacks.forEach(cb => cb(status));
    });
};
export const handleRobotMsg = (callback) => {
    socket.on('msg_rob', msg => callback && callback(msg));
};
export const handleDeviceMessage = (deviceId, callback) => {
    socket.on(`send_device_message:${deviceId}`, (userId, msg) => {
        callback && callback(userId, msg);
    });
};

export const removeDeviceListeter = (deviceId) => {
    socket.off(`send_device_message:${deviceId}`);
};

export const disableUser = (callbacks) => {
    socket.close();
};

export const connectUserAndDevice = (userId, deviceId) => {
    socket.emit('init_user', userId, deviceId);
};

export const sendMsgToDevice = (msg, userId, deviceId) => {
    startRequest(msg, userId, deviceId);
};

export const getUserDescription = (userId) => {
    //hardcoded
    return new Promise((res, rej) => {
        setTimeout(() => {
            res({
                robots: {
                    ids: ['DEVICE_ID', 'DEVICE_ID_STEP'],
                    ['DEVICE_ID_STEP']: { id: 'DEVICE_ID_STEP', type: 'step' },
                    ['DEVICE_ID']: { id: 'DEVICE_ID', type: 'ride' }
                },
                controllers: { default: true }
            });
        }, 1000);
    });
};

export const getDeviceDescription = (deviceId, { token }) => {
    return fetch(`${serverUrl}device?deviceId=${deviceId}`, {
        method: 'GET',
        headers: {
            'x-access-token': token,
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
          }
    });
};