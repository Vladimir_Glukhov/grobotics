import { serverUrl } from '../settings';

export const logIn = (login, password) => {
    return fetch(`${serverUrl}api/login`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({
            login,
            password
        })
    });
};

export const getUser = (token) => {
    return fetch(`${serverUrl}api/user/`, {
        method: 'GET',
        Accept: 'application/json',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'x-access-token': token
        }
    });
};