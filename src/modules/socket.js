import io from 'socket.io-client';
import { socketUrl } from './settings';

const socket = io(socketUrl, {
    withCredentials: true,
    query: { token: 'myToken '},
    transports: ["polling", "websocket"],
    //transportOptions: {
      //  polling: {
    extraHeaders: {
        'my-custom-header': 'Bearer myFirstToken',
    },
        //},
    //},
});

export default socket;