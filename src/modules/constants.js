export const LED_2_ON = 1;
export const LED_3_ON = 5;
export const LED_OFF  = 0;

export const LED_2_ON_ID = 'id1';
export const LED_3_ON_ID = 'id2';
export const LED_OFF_ID = 'id3';

export const MOTOR_1_UP_ID = 'id10';
export const MOTOR_1_DOWN_ID = 'id11';
export const MOTOR_1_STOP_ID = 'id12';
export const MOTOR_2_UP_ID = 'id13';
export const MOTOR_2_DOWN_ID = 'id14';
export const MOTOR_2_STOP_ID = 'id15';

export const MOTOR_1 = 1;
export const MOTOR_2 = 2;

export const totalPower = {
    [MOTOR_1]: 0,
    [MOTOR_2]: 0
};

export const powerSetIds = {
    [MOTOR_1]: [MOTOR_1_UP_ID, MOTOR_1_DOWN_ID],
    [MOTOR_2]: [MOTOR_2_UP_ID, MOTOR_2_DOWN_ID]
};

export const devicesIds = {
    GYRO: 'GYRO',
    MSDIST: 'MSDIST',
    CAM: 'CAM',
    MOT: 'MOT',
    PWR: 'PWR',
    ARMS: 'ARMS',
    SOCKET_CAMERA: 'SOCKET_CAMERA'
};

export const devicesDescriptions = {
    [devicesIds.MOT]: 'Motors',
    [devicesIds.CAM]: 'Camera',
    [devicesIds.MSDIST]: 'Distance sensor',
    [devicesIds.GYRO]: 'Gyroskope',
    [devicesIds.PWR]: 'Power Elements'
};

export const robotsDescriptions = {
    'DEVICE_ID':  'Ride robot',
    'DEVICE_ID_STEP': 'Step robot'
};

export const APP_ROUTES = {
    home: '',
    robots: '/robots',
    contacts: '/contacts',
    roboland: '/roboland',
    login: '/login'
};

export const ROBOLAND_ROUTES = {
    robots: `${APP_ROUTES.roboland}/robots`,
    controllers: `${APP_ROUTES.roboland}/controllers`
};