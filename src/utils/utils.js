export const isMobileDevice = () => window.navigator.userAgent && window.navigator.userAgent.indexOf('Windows') === -1;

export function debounce(func, timer = 100) {
    let time = false;
    return function (...arg) {
        if (!time) {
            time = true;
            func.call(this, ...arg);
            setTimeout(() => { time = false; }, timer);
        }
    };
}

export function throttle(func, timer = 200) {
    let time = false;
    let calledThis = null;
    let calledArgs = null;
    return function (...arg) {
        if (!time) {
            time = true;
            func.call(this, ...arg);
            setTimeout(() => {
                time = false;
                if (calledThis !== null) {
                    func.call(calledThis, ...calledArgs);
                    calledThis = null;
                    calledArgs = null;
                }
            }, timer);
        } else {
            calledThis = this;
            calledArgs = arg;
        }

    };
}