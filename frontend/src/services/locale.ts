import { Property } from "@frp-ts/core";
import { BasePageTexts, Dictionary, Locale } from "../models/models";
import { LocaleRepository } from "../repository/locale.repository";

export interface LocaleService {
  readonly dictionary: Property<Dictionary>;
  readonly setLocale: (l: Locale) => void;
}

export const newLocaleService = (
  repository: LocaleRepository
): LocaleService => ({
  dictionary: repository.dictionary,
  setLocale: repository.setLocale,
});
