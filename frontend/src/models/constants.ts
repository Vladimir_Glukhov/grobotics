import { Pages } from "./models";

export const URLS: Record<Pages, string> = {
    mainPage: '/home',
    contactInfo: '/contact-info',
    roboland: '/roboland'
};