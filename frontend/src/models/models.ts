export interface BasePageTexts {
  readonly title: string;
  readonly desciption: string;
}

export interface MainPageTexts extends BasePageTexts {
  readonly headTitle: string;
  readonly features: {
    readonly f1: string;
    readonly f2: string;
    readonly f3: string;
  }
  readonly titles: {
    readonly t1: string;
  }
}

export type Locale = "EN" | "RU";

export type Pages = "mainPage" | "contactInfo" | "roboland";

export type Dictionary = { locale: Locale } & Record<Pages, BasePageTexts> & {
    readonly mainPage: MainPageTexts;
  };
