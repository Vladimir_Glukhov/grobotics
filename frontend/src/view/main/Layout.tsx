import React, { ReactNode } from "react";
import { Header } from "../pages/components/Header";
import styles from "./styles.module.scss";

interface LayoutProps {
  readonly children: ReactNode;
}

export const Layout = ({ children }: LayoutProps) => {

  return (
    <div className={styles.Layout}>
      <Header />
      {children}
    </div>
  );
};
