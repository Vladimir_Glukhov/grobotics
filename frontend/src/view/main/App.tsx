import React, { memo, useMemo } from "react";
import { LocaleServiceContext } from "../../contexts/locale";
import { newLocaleRepository } from "../../repository/locale.repository";
import { newLocaleService } from "../../services/locale";
import { BRouter, Routes } from "../router/Router";
import { Layout } from "./Layout";

export const App = memo(() => {
  const localeRepository = useMemo(() => newLocaleRepository(), []);
  const localeService = useMemo(
    () => newLocaleService(localeRepository),
    [localeRepository]
  );

  return (
    <BRouter>
      <LocaleServiceContext.Provider value={localeService}>
        <Layout>{...Routes}</Layout>
      </LocaleServiceContext.Provider>
    </BRouter>
  );
});
