import React, { memo, useEffect, useState } from "react";
import cn from "classnames";

import styles from "./styles.module.scss";

interface TopSectionProps {
  readonly state: number;
  readonly texts: {
    readonly features: {
      readonly f1: string;
      readonly f2: string;
      readonly f3: string;
    };
  };
}
export const TopSection = memo(({ state, texts }: TopSectionProps) => {
  const [isReady, setIsReady] = useState(false);
  useEffect(() => {
    setIsReady(true);
  }, []);

  return (
    <section className={styles.Section}>
      <div className={styles["TopSection-img-container"]}>
        <div
          className={cn(styles["TopSection-img"], {
            [styles["TopSection-img-is-ready"]]: isReady,
          })}
        >
          <div>
            {
              <TextWithLine
                className={styles["feature-texts-f1"]}
                text={texts.features.f1}
                visible={state > 0}
                time={100}
              />
            }
            {
              <TextWithLine
                className={styles["feature-texts-f2"]}
                text={texts.features.f2}
                time={100}
                visible={state > 1}
              />
            }
            {
              <TextWithLine
                className={styles["feature-texts-f3"]}
                text={texts.features.f3}
                time={100}
                visible={state > 2}
              />
            }
          </div>
          <div className={styles.VerstionText}>v 0.0.1-alpha</div>
        </div>
      </div>
    </section>
  );
});

interface TextWithLineProps {
  readonly text: string;
  readonly className: string;
  readonly time: number;
  readonly visible: boolean;
}

const TextWithLine = ({ text, visible, className, time }: TextWithLineProps) => {
  const [isStarted, setStarted] = useState(false);
  const [showDescription, setShowDescription] = useState(false);
  useEffect(() => {
    visible && setTimeout(() => {
      setStarted(true);
    }, time);
  }, [visible]);
  useEffect(() => {
    !visible && setStarted(false);
  }, [visible])
  useEffect(() => {
    isStarted && setShowDescription(true);
  }, [isStarted]);
  return (
    <div
      className={cn(
        styles["feature-texts-container"],
        { [styles["feature-texts-container-started"]]: isStarted },
        className
      )}
    >
      <div className={styles["feature-texts-dot"]} />
      <div className={styles["feature-texts-line"]} />
      <p className={styles["feature-texts-text"]}>{text}</p>
      <p
        className={cn(styles["feature-texts-description"], {
          [styles["feature-texts-description-show"]]: showDescription,
        })}
      >
        Super long description. Super long description.
      </p>
    </div>
  );
};
