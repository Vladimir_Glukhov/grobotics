import { useProperty } from "@frp-ts/react";
import React, {
  memo,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import cn from "classnames";
import { LocaleServiceContext } from "../../../contexts/locale";
import { MainPageTexts } from "../../../models/models";
import styles from "./styles.module.scss";

import video1 from "../../../video/v1.mp4";
import video2 from "../../../video/v2.mp4";
import video3 from "../../../video/v3.mp4";
import { TopSection } from "./components/TopSection";

interface MainPageProps {
  readonly texts: MainPageTexts;
}

export const MainPage = memo(() => {
  const localeService = useContext(LocaleServiceContext);
  const dictionary = useProperty(localeService.dictionary);

  return <MainPageComponent texts={dictionary.mainPage} />;
});

const MainPageComponent = memo(({ texts }: MainPageProps) => {
  const [state, setState] = useState(0);

  const handleScroll = useCallback((e: React.UIEvent<HTMLDivElement>) => {
    const { scrollHeight, scrollTop } = e.currentTarget;
    const diff = (scrollTop / scrollHeight) * 100;
    console.log(diff);
    setState(0);
    if (diff > 3) setState(1);
    if (diff > 9) setState(2);
    if (diff > 15) setState(3);
    if (diff > 21) setState(4);
  }, []);

  return (
    <div className={styles.Container}>
      <div className={styles.ScrollContainer} onScrollCapture={handleScroll}>
        <div className={styles["MainPage-head-title"]}>{texts.headTitle}</div>
        <TopSection state={state} texts={texts} />
        <p className={styles["texts-title"]}>{texts.titles.t1}</p>
        <div className={styles["bottom-section"]}>
          <video muted loop autoPlay>
            <source src={video1} />
          </video>
          <video muted loop autoPlay>
            <source src={video3} />
          </video>
          <video muted loop autoPlay>
            <source src={video2} />
          </video>
          <footer className={styles.footer}>by Vladimir Glukhov</footer>
        </div>
      </div>
    </div>
  );
});

interface TextWithLineProps {
  readonly text: string;
  readonly className: string;
  readonly time: number;
}
const TextWithLine = ({ text, className, time }: TextWithLineProps) => {
  const [isStarted, setStarted] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setStarted(true);
    }, time);
  }, []);

  return (
    <div
      className={cn(
        styles["feature-texts-container"],
        { [styles["feature-texts-container-started"]]: isStarted },
        className
      )}
    >
      <div className={styles["feature-texts-dot"]} />
      <div className={styles["feature-texts-line"]} />
      <p className={styles["feature-texts-text"]}>{text}</p>
      <p className={styles["feature-texts-description"]}>
        Super long description
      </p>
    </div>
  );
};
