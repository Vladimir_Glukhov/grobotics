import React, {
  useMemo,
  memo,
  useContext,
  useCallback,
  useEffect,
  useState,
} from "react";
import { useProperty } from "@frp-ts/react";
import { Link } from "react-router-dom";
import { LocaleServiceContext } from "../../../contexts/locale";
import { URLS } from "../../../models/constants";
import { newHeaderViewModel } from "./Header.view-model";
import styles from "./styles.module.scss";
import { Dictionary } from "../../../models/models";
import cn from "classnames";

export const Header = memo(() => {
  const localeService = useContext(LocaleServiceContext);

  const vm = useMemo(
    () => newHeaderViewModel({ localeService }),
    [localeService]
  );
  const texts = useProperty(vm.texts);

  const handleSetLocale = useCallback(() => {
    const currentLocale = texts.locale;
    vm.setLocale(currentLocale === "EN" ? "RU" : "EN");
  }, [texts.locale]);

  return <HeaderComponent texts={texts} setLocale={handleSetLocale} />;
});

interface HeaderComponentProps {
  readonly texts: Dictionary;
  readonly setLocale: () => void;
}

const HeaderComponent = memo(({ texts, setLocale }: HeaderComponentProps) => {
  const [isActive, setActive] = useState<boolean>(true);
  useEffect(() => {
    setTimeout(() => {
      setActive(false);
    }, 1500);
  });

  return (
    <div className={cn(styles.Header)}>
      <MenuLinkItem
        url={URLS.mainPage}
        isActive={isActive}
        title={texts.mainPage.title}
      />
      <MenuLinkItem
        url={URLS.contactInfo}
        isActive={isActive}
        title={texts.contactInfo.title}
      />
      <MenuLinkItem
        url={URLS.roboland}
        isActive={isActive}
        title={texts.roboland.title}
      />
      <button
        className={cn(styles.MenuButton, {
          [styles["MenuButton-en"]]: texts.locale === "EN",
          [styles["MenuButton-ru"]]: texts.locale === "RU",
        })}
        onClick={setLocale}
      />
    </div>
  );
});

interface MenuLinkItemProps {
  readonly url: string;
  readonly classNames?: string;
  readonly title: string;
  readonly isActive: boolean;
}
const MenuLinkItem = ({ url, title, isActive }: MenuLinkItemProps) => (
  <Link
    to={url}
    className={cn(styles.MenuItem, {
      [styles["MenuItem--is-active"]]: isActive,
    })}
  >
    <span>{title[0]}</span>
    <span className={styles.MenuItemSubText}>
      {title.slice(1, title.length)}
    </span>
  </Link>
);
