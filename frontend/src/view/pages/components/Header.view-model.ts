import { Dictionary, Locale } from "../../../models/models";
import { LocaleService } from "../../../services/locale";
import { Property } from "@frp-ts/core";

export interface HeaderViewModel {
  readonly setLocale: (l: Locale) => void;
  readonly texts: Property<Dictionary>;
}

interface HeaderViewModelProps {
  readonly localeService: LocaleService;
}

export const newHeaderViewModel = ({
  localeService,
}: HeaderViewModelProps): HeaderViewModel => {
  const setLocale = (l: Locale) => localeService.setLocale(l);

  return {
    setLocale,
    texts: localeService.dictionary,
  };
};
