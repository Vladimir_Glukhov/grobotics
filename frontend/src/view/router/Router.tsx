import React from "react";
import { BrowserRouter, BrowserRouterProps, Route } from "react-router-dom";
import { URLS } from "../../models/constants";
import { MainPage } from "../pages/MainPage/MainPage";

export const BRouter = ({ children }: BrowserRouterProps) => (
  <BrowserRouter>{children}</BrowserRouter>
);

export const Routes = [
  <Route path={URLS.mainPage}>
    <MainPage />
  </Route>,
  <Route path={URLS.contactInfo}>22</Route>,
  <Route path={URLS.roboland}>33</Route>,
];
