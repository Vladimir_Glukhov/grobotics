import { createContext } from "react";
import { Dictionary } from "../models/models";
import { LocaleService } from "../services/locale";

export const DictionaryContext = createContext<Dictionary>({} as Dictionary);

export const LocaleServiceContext = createContext<LocaleService>({} as LocaleService);
