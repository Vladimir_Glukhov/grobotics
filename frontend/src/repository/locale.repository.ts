import { Property, newAtom } from "@frp-ts/core";
import { Dictionary, Locale } from "../models/models";

export interface LocaleRepository {
  readonly dictionary: Property<Dictionary>;
  readonly setLocale: (l: Locale) => void;
}

const LOCALES: Record<Locale, Dictionary> = {
  EN: {
    mainPage: {
      title: "Home",
      desciption: "My description",
      headTitle: "Robots are our future",
      features: {
        f1: "Internet control",
        f2: "Moved camera",
        f3: "3 motors",
      },
      titles: {
        t1: "We are developing robots. Easy to use, easy to create, make your robot as you wish",
      },
    },
    contactInfo: {
      title: "Cotact Info",
      desciption: "",
    },
    roboland: {
      title: "Roboland",
      desciption: "",
    },
    locale: "EN",
  },
  RU: {
    mainPage: {
      title: "Главная",
      desciption: "Мое описание",
      headTitle: "Будущее рядом",
      features: {
        f1: "Internet control",
        f2: "Moved camera",
        f3: "3 motors",
      },
      titles: {
        t1: "We are developing robots. Easy to use, easy to create, make your robot as you wish",
      },
    },
    contactInfo: {
      title: "Контактная информация",
      desciption: "",
    },
    roboland: {
      title: "Roboland",
      desciption: "",
    },
    locale: "RU",
  },
};

export const newLocaleRepository = (): LocaleRepository => {
  const locale = newAtom<Locale>("EN");
  const d = newAtom<Dictionary>(LOCALES["EN"]);

  locale.subscribe({
    next() {
      d.set(LOCALES[locale.get()]);
    },
  });

  return {
    dictionary: d,
    setLocale: locale.set,
  };
};
