const express = require('express');
const path = require('path');

const app = express();
const server = require('http').createServer(app);

const io = require('socket.io')(server, {
  cors: {
    origin: ['http://localhost:8080'],
    allowedHeaders: ['my-custom-header'],
    methods: ['GET', 'POST'],
    transports: ['polling', 'websocket'],
    credentials: true,
  },
});

const { connectedDevicesSocketId, connectedUsersSocketId, accessToUser } = require('./api');

const connectedDevicesIds = {};
const connectedDevicesSets = {};
console.log('socket server started');

io.use((socket, next) => {
  const header = socket.handshake.headers['my-custom-header'];
  const token = header?.split(' ')[1];
  console.log(token);
  if (token === 'blabla') {
    console.log('is valid token', socket.handshake);
    next();
  } else {
    console.log('is invalid token', socket.handshake);
  }
});
io.on('connection', (socket) => {
  console.log('connected', socket.id);
  console.log(socket.handshake);
  socket.on('send', async (userId, deviceId, msg) => {
    const devSocketId = await connectedDevicesSocketId.get(deviceId);

    if (!devSocketId) {
      console.log('device not found', userId, deviceId);
      return;
    }

    const userIdSuccess = await accessToUser.getAccess(deviceId);

    if (userIdSuccess === userId) {
      socket.to(devSocketId).emit('to_dev', msg);
    } else {
      console.log('access denied', userId, deviceId);
    }
  });

  socket.on('device_connect', async (devId, description) => {
    connectedDevicesIds[socket.id] = devId;
    connectedDevicesSets[devId] = description;

    setDeviceStatus(socket, devId, true);

    return connectedDevicesSocketId.add(devId, socket.id);
  });

  socket.on('init_user', async (userId, devId) => {
    await connectedUsersSocketId.add(userId, socket.id);
    const devSocketId = await connectedDevicesSocketId.get(devId);

    if (devSocketId) {
      socket.to(devSocketId).emit('init_user', userId);
    } else {
      console.log('no devices to init user');
    }
  });

  socket.on('init_user_success', async (userId, devId) => {
    await accessToUser.addAccess(devId, userId);
    const userSocketId = await connectedUsersSocketId.get(userId);
    const devSocketId = await connectedDevicesSocketId.get(devId);

    console.log('init_user_success', devSocketId);

    socket.to(userSocketId).emit('set_user_status', true);
  });

  socket.on('msg_from_device', async (userId, msg) => {
    const userSocketId = await connectedUsersSocketId.get(userId);
    socket.to(userSocketId).emit(`send_device_message:${msg.deviceId}`, userId, msg);
  });

  socket.on('msg_rob', async (userId, msg) => {
    const userSocketId = await connectedUsersSocketId.get(userId);
    socket.to(userSocketId).emit('msg_rob', msg);
  });

  socket.on('disconnect', async () => {
    console.log('disconnected', socket.id);
    const devId = connectedDevicesIds[socket.id];
    delete connectedDevicesSets[socket.id];

    connectedUsersSocketId.delete(socket.id);
    setDeviceStatus(socket, devId, false);
  });
});

async function setDeviceStatus(socket, devId, status) {
  const userIdSuccess = await accessToUser.getAccess(devId);
  const userSocketId = await connectedUsersSocketId.get(userIdSuccess);
  socket.to(userSocketId).emit('set_user_status', status);
}

const port = process.env.PORT || 3030;
const myPath = express.static(path.join(__dirname, 'build'));

app.use(myPath);
server.listen(port);
