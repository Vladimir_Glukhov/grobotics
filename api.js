const connectedDevicesList = new Map();
const connectedUsersList = new Map();
const accessToUserList = new Map();
const usersSockerIds = {};

const connectedDevicesSocketId = {
    async add(devId, socketId) {
        return await connectedDevicesList.set(devId, socketId);
    },
    async get(devId) {
        return await connectedDevicesList.get(devId) || null
    }
};

const connectedUsersSocketId = {
    async add(devId, socketId) {
        usersSockerIds[socketId] = true;
        return await connectedUsersList.set(devId, socketId);
    },
    async get(devId) {
        return await connectedUsersList.get(devId) || null
    },
    async delete(socketId) {
        if (usersSockerIds[socketId]) {
            delete usersSockerIds[socketId];
        }
    },
    getAll() {
        return Object.keys(usersSockerIds);
    }
};

const accessToUser = {
    async addAccess(devId, userId) {
        await accessToUserList.set(devId, userId);
    },
    async getAccess(devId) {
        return await accessToUserList.get(devId)
    }
};

module.exports = {
    connectedDevicesSocketId,
    connectedUsersSocketId,
    accessToUser
}